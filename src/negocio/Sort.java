package negocio;

import java.util.Arrays;

/**
 *
 * @author Nocsabe
 */
public class Sort {

    int contadorShell;
    int contadorShellOptimizado;

    public Sort() {
        this.contadorShell = 0;
        this.contadorShellOptimizado = 0;
    }

    public void shellSortOptimizado(int arr2[]) {
        int n = arr2.length;
        this.contadorShellOptimizado = 0;
        int arr[] = Arrays.copyOf(arr2, n);

        for (int gap = n / 2; gap > 0; gap /= 2) {
            if (arr[gap] % 2 == 0) {
                gap -= 1;
            }
            for (int i = gap; i < n; i += 1) {
                int temp = arr[i];
                int j;
                contadorShellOptimizado++;
                for (j = i; j >= gap && arr[j - gap] > temp; j -= gap) {
                    arr[j] = arr[j - gap];
                }
                arr[j] = temp;
            }
        }       
    }

    public void shellSort(int arr2[]) {
        int n = arr2.length;
        int arr[] = Arrays.copyOf(arr2, n);
        this.contadorShell = 0;
        for (int gap = n / 2; gap > 0; gap /= 2) {
            for (int i = gap; i < n; i += 1) {
                int temp = arr[i];
                int j;
                contadorShell++;
                for (j = i; j >= gap && arr[j - gap] > temp; j -= gap) {
                    arr[j] = arr[j - gap];
                    contadorShell++;
                }
                arr[j] = temp;
            }
        }
    }

    public int getContadorShell() {
        return contadorShell;
    }

    public void setContadorShell(int contadorShell) {
        this.contadorShell = contadorShell;
    }

    public int getContadorShellOptimizado() {
        return contadorShellOptimizado;
    }

    public void setContadorShellOptimizado(int contadorShellOptimizado) {
        this.contadorShellOptimizado = contadorShellOptimizado;
    }

}
