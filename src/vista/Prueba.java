package vista;

import java.util.ArrayList;
import negocio.Sort;

/**
 *
 * @author Nocsabe
 */
public class Prueba {

    public static void pruebaIteraciones(ArrayList<int[]> casosPrueba, int[] tamaño) {
        System.out.println("PRUEBA DE NRO DE ITERACIONES");
        System.out.println("");
        for (int i = 0; i < casosPrueba.size(); i++) {
            Sort sort = new Sort();
            sort.shellSort(casosPrueba.get(i));
            sort.shellSortOptimizado(casosPrueba.get(i));
            System.out.println("-- NRO DE VALORES: " + tamaño[i] + " --");
            System.out.println("Nro iteraciones ShellSort: " + sort.getContadorShell());
            System.out.println("Nro iteraciones ShellSortOptimizado: " + sort.getContadorShellOptimizado());
            System.out.println("Nro de iteraciones que se ahorro: " + (sort.getContadorShell() - sort.getContadorShellOptimizado()));
            System.out.println("");
        }
    }

    public static void pruebaTiempo(ArrayList<int[]> casosPrueba, int[] tamaño) {
        long time_start, time_end;
        System.out.println("PRUEBA DE TIEMPO");
        System.out.println("");
        for (int i = 0; i < casosPrueba.size(); i++) {
            Sort sort = new Sort();
            System.out.println("-- NRO DE VALORES: " + tamaño[i] + " --");

            time_start = System.currentTimeMillis();
            sort.shellSort(casosPrueba.get(i));
            time_end = System.currentTimeMillis();
            System.out.println("Tiempo ShellSort: " + (time_end - time_start) + " milisegundos");

            time_start = System.currentTimeMillis();
            sort.shellSortOptimizado(casosPrueba.get(i));
            time_end = System.currentTimeMillis();
            System.out.println("Tiempo ShellSort optimizado: " + (time_end - time_start) + " milisegundos");
            System.out.println("");

        }
    }

    public static void main(String[] args) {
        int tamaño[] = {10, 50000, 100000, 150000, 200000, 250000, 300000, 350000, 400000, 450000, 500000, 550000, 600000, 650000, 700000, 750000, 800000};

        ArrayList casosPrueba = new ArrayList<>();
        int[] secPeor;
        int poscPar;
        int poscImpar;

        for (int i = 0; i < tamaño.length; i++) {
            poscPar = 1;
            poscImpar = (tamaño[i] / 2) + 1;
            secPeor = new int[tamaño[i]];
            for (int j = 0; j < tamaño[i]; j++) {
                if (j % 2 == 0) {
                    secPeor[j] = poscPar;
                    poscPar += 1;
                } else {
                    secPeor[j] = poscImpar;
                    poscImpar += 1;
                }
            }
            casosPrueba.add(secPeor);
        }
        System.out.println("-- OPTIMIZACION DEL SHELL SORT PEOR CASO --");
        System.out.println("(En una secuencia ordenada ascendentemente, la partimos en dos,luego, los valores de la mitad superior \n"
                + "irian en posiciones pares del arreglo, y en las posiciones impares los valores de la mitad inferior del arreglo.)");
        System.out.println("EJEMPLO: 1, 6, 2, 7, 3, 8, 4, 9, 5, 10");
        System.out.println("");

        pruebaIteraciones(casosPrueba, tamaño);
        //pruebaTiempo(casosPrueba, tamaño);               
    }
}
